#!/usr/bin/env bash

# Version: 2024.07.01

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

# source cfgs
source libs/pkgs.conf
source libs/general.conf
source libs/installed_arch_setup.conf
source libs/chroot_common.conf

# some exports
export OS_DISK="/dev/XXX"
export OS_DISK_TYPE_NVME="no"
export BOOT_PART="1"
export POOL_PART="2"
export BOOT_DEVICE="${OS_DISK}${BOOT_PART}"
export POOL_DEVICE="${OS_DISK}${POOL_PART}"
export MNT_CHROOT="/mnt"
export DISTRO="archlinux"
export ENCRYPT_ZFS="no"
export ZFS_KEY_LOCATION="prompt"
export ZFS_KEY_FORMAT="passphrase"
# export ZFS_KEY_LOCATION="file:///etc/zfs/${ZPOOL_NAME}.key"
# export ZFS_KEY_FILE="/etc/zfs/${ZPOOL_NAME}.key"
export ZPOOL_NAME="aroot"
export VCONSOLE_KEYMAP_TOGGLE="de-latin1-nodeadkeys"
export VCONSOLE_KEYMAP="us"
export SD_ADDRESS="192.168.77./24"
export SD_GATEWAY="192.168.77.1"
export SD_DOMAINS="dreaming.void"
export SD_DNS1="192.168.77.1"
export SD_DNS2="9.9.9.9"
export LANG="en_US.UTF-8"
export TIME_ZONE="Europe/Berlin"
export USERNAME="apovis"
export REMOVE_PREVIOUS_ZBM_ENTRIES="yes"

# add private arch zfs repo "zerope"
echo '
[zerope]
SigLevel = Optional TrustAll
#Server = https://pie3.dreaming.void/zerope
Server = https://www.grafmurr.de/zerope
' >> /etc/pacman.conf

# remove zfs related disk layouts, ools and datasets THEN wipe disk
umount -nR ${MNT_CHROOT} || echo "${MNT_CHROOT} is not mounted"
zfs umount -af
zpool offline -f ${ZPOOL_NAME} ${POOL_DEVICE} || echo "no pool to take offline"
zpool labelclear -f ${ZPOOL_NAME} || echo "no labels to be cleaned"
zpool destroy -f ${ZPOOL_NAME} || echo "no pools to destroy"
wipefs -a "${OS_DISK}"

if [ ${OS_DISK_TYPE_NVME} == "no" ]; then
  echo "no actions needed, since this is not a nvme"

elif [ ${OS_DISK_TYPE_NVME} == "yes" ]; then
  nvme id-ns ${OS_DISK} -H | grep "LBA Format"
  echo "which lba mode to use ?"
  read LBA_MODE
  nvme format ${OS_DISK} -l ${LBA_MODE}

else
  echo "please use yes or no for OS_DISK_TYPE_NVME"
  exit 1
fi

if [ ${REMOVE_PREVIOUS_ZBM_ENTRIES} == "no" ]; then
  echo "letting efivars entries untouched"

elif [ ${REMOVE_PREVIOUS_ZBM_ENTRIES} == "yes" ]; then
  echo "removing old ZFSBootMenu entries from efivars"
  for i in $(efibootmgr | grep ZFSBootMenu | cut -d"*" -f1 | xargs); do efibootmgr -B -b ${i##*t}; done

else
  echo "please use yes or no for REMOVE_PREVIOUS_ZBM_ENTRIES"
  exit 1
fi

# create new gpt partition table
sgdisk -Z "${OS_DISK}"

# create 2 partitions esp and os
sgdisk -n "${BOOT_PART}:0:+512m" -t "${BOOT_PART}:ef00" "${OS_DISK}"
sgdisk -n "${POOL_PART}:0:-10m" -t "${POOL_PART}:bf00" "${OS_DISK}"

# generate hostid for zfs
zgenhostid -f

if [ ${ENCRYPT_ZFS} == "no" ]; then
  # create zpool
  zpool create -f \
    -o ashift=12 \
    -O acltype=posixacl \
    -O canmount=off \
    -O dnodesize=auto \
    -O relatime=on \
    -O xattr=sa \
    -O compression=zstd-6 \
    -o autotrim=on \
    -O normalization=formD \
    -O devices=off \
    -m none -R ${MNT_CHROOT} ${ZPOOL_NAME} ${POOL_DEVICE}

  # create datasets
  zfs create -o canmount=off -o mountpoint=none ${ZPOOL_NAME}/ROOT
  zfs create -o canmount=noauto -o mountpoint=/ ${ZPOOL_NAME}/ROOT/${DISTRO}
  zfs create -o canmount=off -o mountpoint=none ${ZPOOL_NAME}/home
  zfs create -o canmount=on -o compression=zstd-6 -o mountpoint=/root ${ZPOOL_NAME}/home/root

  zpool set bootfs=${ZPOOL_NAME}/ROOT/${DISTRO} ${ZPOOL_NAME}

  zpool export ${ZPOOL_NAME}
  zpool import -N -R ${MNT_CHROOT} ${ZPOOL_NAME}
  zfs mount ${ZPOOL_NAME}/ROOT/${DISTRO}
  zfs mount -a

elif [ ${ENCRYPT_ZFS} == "yes" ]; then
  # create zpool
  zpool create -f -o ashift=12 \
    -O acltype=posixacl \
    -O canmount=off \
    -O dnodesize=auto \
    -O relatime=on \
    -O xattr=sa \
    -O compression=zstd-6 \
    -o autotrim=on \
    -O normalization=formD \
    -O encryption=aes-256-gcm \
    -O keylocation="${ZFS_KEY_LOCATION}" \
    -O keyformat="${ZFS_KEY_FORMAT}" \
    -O devices=off \
    -m none -R ${MNT_CHROOT} ${ZPOOL_NAME} ${POOL_DEVICE}

  # create datasets
  zfs create -o canmount=off -o mountpoint=none ${ZPOOL_NAME}/ROOT
  zfs create -o canmount=noauto -o mountpoint=/ ${ZPOOL_NAME}/ROOT/${DISTRO}
  zfs create -o canmount=off -o mountpoint=none ${ZPOOL_NAME}/home
  zfs create -o canmount=on -o mountpoint=/root ${ZPOOL_NAME}/home/root

  zpool set bootfs=${ZPOOL_NAME}/ROOT/${DISTRO} ${ZPOOL_NAME}

  zpool export ${ZPOOL_NAME}
  zpool import -N -R ${MNT_CHROOT} ${ZPOOL_NAME}
  zfs load-key -L prompt ${ZPOOL_NAME}
  zfs mount ${ZPOOL_NAME}/ROOT/${DISTRO}
  zfs mount -a

else
  echo "please use yes or no for ZFS_ENCRYPT variable"
  exit 1
fi

# format and mount /boot/efi
mkfs.vfat -F32 ${BOOT_DEVICE}
mkdir -p ${MNT_CHROOT}/boot/efi
mount ${BOOT_DEVICE} ${MNT_CHROOT}/boot/efi

# mount efivars
mount -t efivarfs efivarfs /sys/firmware/efi/efivars || echo "already mounted"

# install archlinux
pacstrap -P ${MNT_CHROOT} ${ARCH_SERVER_PKG_ZFS}

# copy zhostid to chroot
cp /etc/hostid ${MNT_CHROOT}/etc

# resolved setup
setup_systemd_resolved

# mkinitcpio setup
cp ${MNT_CHROOT}/etc/mkinitcpio.conf{,.orig}
cp files/archlinux/mkinitcpio.conf ${MNT_CHROOT}/etc/mkinitcpio.conf
sed -i "s|<WILL-BE-CHANGED>|base udev autodetect microcode modconf kms keyboard keymap consolefont block zfs filesystems|" "${MNT_CHROOT}/etc/mkinitcpio.conf"

# local time, keymaps and vconsole setup
ln -sf /usr/share/zoneinfo/${TIME_ZONE} ${MNT_CHROOT}/etc/localtime
echo "LANG=${LANG}" >${MNT_CHROOT}/etc/locale.conf
echo "LC_COLLATE=C" >>${MNT_CHROOT}/etc/locale.conf
echo "KEYMAP=${VCONSOLE_KEYMAP}" >${MNT_CHROOT}/etc/vconsole.conf
echo "KEYMAP_TOGGLE=${VCONSOLE_KEYMAP_TOGGLE}" >${MNT_CHROOT}/etc/vconsole.conf
echo "FONT=Lat2-Terminus16" >>${MNT_CHROOT}/etc/vconsole.conf

# sshd config
copy_ssh_files

# create doas config
setup_doas

# run script in chroot
cp archlinux_02_zfs_chroot.sh ${MNT_CHROOT}/root/
arch-chroot ${MNT_CHROOT} /root/archlinux_02_zfs_chroot.sh

# umount chroot
umount -n -R ${MNT_CHROOT}
zfs umount -a

# unmount and export zpool
zpool export -a
reboot
