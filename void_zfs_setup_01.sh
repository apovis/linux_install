#!/bin/bash -x

# Version: 2022.11.23

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob

# not for chroot

# source
# https://github.com/zbm-dev/zfsbootmenu/wiki/Void-Linux---Single-disk-UEFI

# general
export REPO="https://alpha.de.repo.voidlinux.org/current"
export ARCH="x86_64"

export XBPS_ARCH="${ARCH}"
export XBPS_INSTALL="$(which xbps-install)"
${XBPS_INSTALL} -Syu xbps
${XBPS_INSTALL} -S gptfdisk parted vim curl xtools git efibootmgr

export PARTD="$(which parted)"
export SGDISK="$(which sgdisk)"
export MNT_CHROOT="/mnt"
export PARTPROBE="$(which partprobe)"

export ZPOOL_NAME="zroot"
export ZFS_PASSWD="/etc/zfs/${ZPOOL_NAME}.key"

VOID_BASE_PKG="base-system zfs zfsbootmenu refind cryptsetup efibootmgr efivar tar bsdtar dust procs opendoas dracut dracut-uefi cronie xz bash-completion tree \
vim tmux runit runit-void openssh btrfs-progs zstd rsync curl wget iputils parted xbps xtools git tig linux linux-headers linux-firmware-network linux-firmware-intel"

#.Build and load ZFS modules
#xbps-reconfigure -a
#modprobe zfs

#.Generate /etc/hostid
#zgenhostid -f $(hexdump -vn4 -e'4/4 "%08X" 1 "\n"' /dev/urandom)
zgenhostid -f

#.Store your pool passphrase in a key file
#->Encrypted
echo "Set ZFS passphrase"
read -r -p "> ZFS passphrase: " -s zroot_pwd
echo
echo "${zroot_pwd}" > ${ZFS_PASSWD}
cat ${ZFS_PASSWD}
chmod 000 ${ZFS_PASSWD}

select DRIVE in $(ls /dev/disk/by-id/);
do
  export HDD="/dev/disk/by-id/${DRIVE}"
  echo "${HDD}" > hdd_linux
  echo "OS will be installed on ${HDD}"
  dd if=/dev/zero of="${HDD}" bs=8192 count=1
  break
done

# partitioning
${SGDISK} -Z ${HDD}
${PARTD} -s ${HDD} mklabel gpt
${SGDISK} -n1:1M:+512M -t1:ef00 -c 1:"efi4void" "${HDD}"
${SGDISK} -n2:0:+4096M -c 2:"swappy" -t2:8200 "${HDD}"
${SGDISK} -n3:0:0 -t3:bf01 -c 3:"zfs_system" "${HDD}"
${PARTD} -s ${HDD} set 1 esp on
${PARTPROBE} ${HDD}
udevadm settle

# define labels
export EFI_HDD="$(blkid -s PARTUUID -o value ${HDD}-part1)"
export SWAPPY="$(blkid -s PARTUUID -o value ${HDD}-part2)"
export ZFS_SYSTEM="$(blkid -s PARTUUID -o value ${HDD}-part3)"

zpool labelclear -f ${HDD}-part1 || echo "no zfs label on boot"
zpool labelclear -f ${HDD}-part2 || echo "no zfs label on swap"
zpool labelclear -f ${HDD}-part3 || echo "no zfs label on root"

#.Create the zpool
#-> Encrypted
zpool create -f -o ashift=12 \
 -O compression=zstd \
 -O acltype=posixacl \
 -O xattr=sa \
 -O relatime=on \
 -O encryption=aes-256-gcm \
 -O keylocation=file://"${ZFS_PASSWD}" \
 -O keyformat=passphrase \
 -o autotrim=on \
 -O normalization=formD \
 -O devices=off \
 -m none \
 -R ${MNT_CHROOT} \
 ${ZPOOL_NAME} ${ZFS_SYSTEM}

# Create our initial file systems
zfs create -o mountpoint=none ${ZPOOL_NAME}/ROOT
zfs create -o mountpoint=/ -o canmount=noauto ${ZPOOL_NAME}/ROOT/void
zfs create -o mountpoint=/home ${ZPOOL_NAME}/home
zfs create -o mountpoint=/root ${ZPOOL_NAME}/home/root

#.Export, then re-import with a temporary mountpoint of /mnt
#-> Encrypted
zpool export ${ZPOOL_NAME}
zpool import -N -R ${MNT_CHROOT} ${ZPOOL_NAME}
zfs load-key -L prompt ${ZPOOL_NAME}
zfs mount ${ZPOOL_NAME}/ROOT/void
zfs mount ${ZPOOL_NAME}/home
zfs mount ${ZPOOL_NAME}/home/root

#.Verify that everything is mounted correctly
mount | grep ${MNT_CHROOT}

# install voidlinux
${XBPS_INSTALL} -Sy -r ${MNT_CHROOT} -R ${REPO} ${VOID_BASE_PKG}

#.Copy our files into the new install
#-> Encrypted
cp /etc/hostid ${MNT_CHROOT}/etc
cp /etc/resolv.conf ${MNT_CHROOT}/etc/
mkdir -p ${MNT_CHROOT}/etc/zfs
cp ${ZFS_PASSWD} ${MNT_CHROOT}/etc/zfs/

# copy 2nd script to chroot
cp void_zfs_setup_02.sh hdd_linux ${MNT_CHROOT}/root/

echo "sleeping until chroot is ready ;-) ... zzz ..."

# Chroot into the new OS
mount -t proc proc ${MNT_CHROOT}/proc
mount -t sysfs sys ${MNT_CHROOT}/sys
mount -B /dev ${MNT_CHROOT}/dev
mount -t devpts pts ${MNT_CHROOT}/dev/pts
chroot ${MNT_CHROOT} /bin/bash

# unmount everything
umount -R ${MNT_CHROOT}
zfs umount -a

# export zpool and reboot
zpool export -a
poweroff

exit 0