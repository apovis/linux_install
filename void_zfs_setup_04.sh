#!/bin/bash -x

# Version: 2022.11.19

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob

# not for chroot

# umount chroot
umount -R /mnt

# .Export the zpool and reboot
zpool export zroot
reboot