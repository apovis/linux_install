#!/bin/bash

# Version: 2024.01.12

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

disk_prep(){
  source host.conf  
  source libs/general.conf       
  source libs/partitioning.conf    
}

source_configs(){                      
# source general settings and host.conf
  source libs/pkgs.conf    
  source libs/installed_arch_setup.conf  
  source libs/zfs.conf
  source libs/chroot_common.conf
}

# run all functions 
disk_prep
export_vars
get_netif_mac
select_os_drive
prep_arch_iso_for_setup
prep_zfs_for_setup
load_keys
source_configs
set_proxy
zfs_format
create_zpool
create_datasets
export_import_zfs_pool
install_pkgs
copy_files_to_chroot
copy_linux_install_to_chroot
setup_doas
copy_arch_files_to_chroot
replace_arch_mkinitcpio
set_arch_etc_configs
zbm_keymap
format_config_mount_efi
encrypted_swap
chroot_arch_zfs
lights_out
