#!/bin/bash

# Version: 2024.06.24

set -o nounset
set -o errexit
set -x
#set -o noclobber
#set -o noglob


# set doas.conf
echo "permit persist keepenv :wheel" > /etc/doas.conf

# users and pwds
id -u ${USERNAME} &>/dev/null || useradd -m -s /bin/bash -G wheel -U ${USERNAME}
#echo "${USERNAME}:${USERNAME_PASSWD}" | chpasswd -c SHA512 -s 195000 
echo "${USERNAME}:${USERNAME_PASSWD}" | chpasswd -c YESCRYPT -s 195000 
#echo "root:${ROOT_PASSWD}" | chpasswd -c SHA512 -s 195000
echo "root:${ROOT_PASSWD}" | chpasswd -c YESCRYPT -s 195000

# mount efivars
#mount -t efivarfs none /sys/firmware/efi/efivars || echo "already mounted"
echo "efivarfs /sys/firmware/efi/efivars efivarfs defaults 0 0" >> /etc/fstab

# update trust store
update-ca-trust

# set locales
echo '
de_DE.UTF-8 UTF-8  
de_DE ISO-8859-1  
de_DE@euro ISO-8859-15  
en_US.UTF-8 UTF-8  
en_US ISO-8859-1 
' >> /etc/locale.gen
locale-gen

# create repo-files
echo '
#[archzfs]
#Include = /etc/pacman.d/zfsmirror

[zerope]
SigLevel = Optional TrustAll
Server = https://www.grafmurr.de/zerope/
' >> /etc/pacman.conf

echo '
# Origin Server - France
Server = http://archzfs.com/$repo/x86_64
# Mirror - Germany
#Server = http://mirror.sum7.eu/archlinux/archzfs/$repo/x86_64
# Mirror - Germany
Server = https://mirror.biocrafting.net/archlinux/archzfs/$repo/x86_64
# Mirror - India
Server = https://mirror.in.themindsmaze.com/archzfs/$repo/x86_64
# Mirror - US
Server = https://zxcvfdsa.com/archzfs/$repo/$arch
' > /etc/pacman.d/zfsmirror

# add gpg-key for repo-files and sign it
curl -o /tmp/archzfs.gpg https://archzfs.com/archzfs.gpg
pacman-key --add /tmp/archzfs.gpg
pacman-key --lsign-key DDF7DB817396A49B2A2723F7403BD972F75D9D76

# install zfs related tools
pacman -Sy --needed --noconfirm zfsbootmenu efibootmgr zfs-dkms zfs-utils refind linux-lts linux-lts-headers

#.To more quickly discover and import pools on boot, we need to set a pool cachefile
zpool set cachefile=/etc/zfs/zpool.cache ${ZPOOL_NAME}

#.Configure our default boot environment
zpool set bootfs=${ZPOOL_NAME}/ROOT/${DISTRO} ${ZPOOL_NAME}

#.Install and configure ZFSBootMenu
zfs set org.zfsbootmenu:commandline="rw zbm.prefer=${ZPOOL_NAME} zbm.timeout=5" ${ZPOOL_NAME}/ROOT

echo '
Global:
  ManageImages: true
  BootMountPoint: /boot/efi
  DracutConfDir: /etc/zfsbootmenu/dracut.conf.d
  PreHooksDir: /etc/zfsbootmenu/generate-zbm.pre.d
  PostHooksDir: /etc/zfsbootmenu/generate-zbm.post.d
  InitCPIO: true
  InitCPIOConfig: /etc/zfsbootmenu/mkinitcpio.conf
Components:
  ImageDir: /boot/efi/EFI/archlinux
  Versions: 3
  Enabled: true
  syslinux:
    Config: /boot/syslinux/syslinux.cfg
    Enabled: false
EFI:
  ImageDir: /boot/efi/EFI/archlinux
  Versions: false
  Enabled: true
Kernel:
  CommandLine: rw
' > /etc/zfsbootmenu/config.yaml

# copy mkinitcpio-files
mv /etc/mkinitcpio_zfs.conf /etc/mkinitcpio.conf
mv /etc/zfsbootmenu/mkinitcpio_zbm.conf /etc/zfsbootmenu/mkinitcpio.conf

# refind-install
[[ -f /boot/refind_linux.conf ]] && rm /boot/refind_linux.conf

# Create /boot/efi/EFI/archlinux/refind_linux.conf:
#echo '"Boot default"  "zbm.prefer='"'${ZPOOL_NAME}'"' ro quiet loglevel=0 zbm.skip"' > /boot/efi/EFI/archlinux/refind_linux.conf
#echo '"Boot to menu"  "zbm.prefer='"'${ZPOOL_NAME}'"' ro quiet loglevel=0 zbm.show"' >> /boot/efi/EFI/archlinux/refind_linux.conf
#echo "Boot default  zbm.prefer=${ZPOOL_NAME} rw loglevel=0 zbm.skip" > /boot/efi/EFI/archlinux/refind_linux.conf
#echo "Boot to menu  zbm.prefer=${ZPOOL_NAME} rw loglevel=0 zbm.show" >> /boot/efi/EFI/archlinux/refind_linux.conf

# Create /boot/efi/EFI/archlinux/refind_linux.conf
cat << EOF > /boot/efi/EFI/${DISTRO}/refind_linux.conf
timeout 0
"Boot default"  "zbm.prefer=${ZPOOL_NAME} rw loglevel=0 zbm.skip"
"Boot to menu"  "zbm.prefer=${ZPOOL_NAME} rw loglevel=0 zbm.show"
EOF

# enable services
systemctl enable zfs.target zfs-import-cache.service zfs-mount.service zfs-import.target sshd.service systemd-networkd.service

# generate initrd
mkinitcpio -P

# generate zbm image
generate-zbm

exit 0