# linux_install
scripts and functions to setup:
- arch
- void
- alpine

get repo:<br>
git clone https://gitlab.com/apovis/linux_install.git
cd linux_install

set all needed variables in:<br>
host.conf

and run in the same directory:<br>
./install_arch.sh

in chroot:<br>
cd /root/linux_install<br>
./chroot_arch.sh
