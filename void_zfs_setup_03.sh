#!/bin/bash -x

# Version: 2022.11.20

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob

# for chroot

# variables

# source
# https://github.com/zbm-dev/zfsbootmenu/wiki/Void-Linux---Single-disk-UEFI

# general
HOST_NAME="x270"


# .Disable GPU drivers
echo 'omit_drivers+=" amdgpu radeon nvidia nouveau i915 "' >> /etc/zfsbootmenu/dracut.conf.d/drivers.conf

#.Generate the initial ZFSBootMenu initramfs
xbps-reconfigure -f zfsbootmenu
# zfsbootmenu: configuring ...
# Creating ZFS Boot Menu 0.8.1_1, with vmlinuz 5.4.15_1
# Found 0 existing images, allowed to have a total of 3
# Created /boot/efi/EFI/void/vmlinuz-0.8.1_1, /boot/efi/EFI/void/initramfs-0.8.1_1.img

# .Download pre-built binaries
mkdir -p /boot/efi/EFI/void
#cd /boot/efi/EFI/void
#curl -LJO https://get.zfsbootmenu.org/efi

# .Install rEFInd
#-> rEFInd should automatically identify /boot/efi as your EFI partition and install itself accordingly.
refind-install
rm /boot/refind_linux.conf

# .Create /boot/efi/EFI/void/refind_linux.conf:
cat << EOF > /boot/efi/EFI/void/refind_linux.conf
"Boot default"  "zbm.prefer=zroot quiet loglevel=0 zbm.skip"
"Boot to menu"  "zbm.prefer=zroot quiet loglevel=0 zbm.show"
EOF

efibootmgr -c -d ${HDD} -p 1 -L "ZFSBootMenu" -l '\EFI\void\vmlinuz-linux.EFI'
efibootmgr -c -d ${HDD} -p 1 -L "ZFSBootMenu" -l '\EFI\void\zfsbootmenu-release-vmlinuz-x86_64.EFI'

exit 0