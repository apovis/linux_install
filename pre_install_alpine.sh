#!/bin/ash -x

# Version: 2022.10.25

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob


source_configs(){                      
# source general settings and host.conf
  source host.conf                     
  source libs/general.conf             
  source libs/partitioning.conf        
  source libs/pkgs.conf    
  source libs/alpine.conf  
  source libs/installed_common_setup.conf  
  source libs/installed_alpine_setup.conf  
}


# run all functions 
source_configs
export_variables
setup_base_system
install_pkgs