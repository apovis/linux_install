#!/bin/bash

# Version: 2024.06.13

set -o nounset
set -o errexit
set -x
#set -o noclobber
#set -o noglob

# enable services
ln -s /etc/sv/sshd /etc/runit/runsvdir/default/
ln -s /etc/sv/openntpd /etc/runit/runsvdir/default/
ln -s /etc/sv/crond /etc/runit/runsvdir/default/
ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
ln -s /etc/sv/acpid /etc/runit/runsvdir/default/
ln -s /etc/sv/socklog-unix /etc/runit/runsvdir/default/

if [[ ${LIBC_VARIANT} == "glibc" ]]
then
  ${XBPS_INSTALL} -S glibc-locales
  echo 'en_US.UTF-8 UTF-8
en_US ISO-8859-1
de_DE.UTF-8 UTF-8
de_DE ISO-8859-1
de_DE@euro ISO-8859-15
  ' >> /etc/default/libc-locales
  #xbps-reconfigure -f glibc-locales
elif [[ ${LIBC_VARIANT} == "musl" ]]
then
  echo "no glibc needed"
else
  echo "something is broken here."
  exit 1
fi

echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# change root's shell
chsh -s /bin/bash root

#.Set a root password
echo "root:${ROOT_PASSWD}" | chpasswd -c SHA512 -s 195000

# new user
id -u ${USERNAME} &>/dev/null || useradd -m -s /bin/bash -G wheel,users -U ${USERNAME}
echo "${USERNAME}:${USERNAME_PASSWD}" | chpasswd -c SHA512 -s 195000 
#useradd -d /home/${USERNAME} -s /bin/bash -G wheel,users,audio,video,storage,network,scanner,_seatd ${USERNAME}

# mount efivars
#mount -t efivarfs none /sys/firmware/efi/efivars || echo "already mounted"
echo "efivarfs /sys/firmware/efi/efivars efivarfs defaults 0 0" >> /etc/fstab

${XBPS_INSTALL} -Sy zfsbootmenu refind

#.To more quickly discover and import pools on boot, we need to set a pool cachefile
zpool set cachefile=/etc/zfs/zpool.cache ${ZPOOL_NAME}

#.Configure our default boot environment
zpool set bootfs=${ZPOOL_NAME}/ROOT/void ${ZPOOL_NAME}

#.Configure Dracut to load ZFS support
if [[ "${DO_ZFS_ENCRYPT}" == "yes" ]] && [[ "${ZFS_KEY_LOCATION}" == "prompt" ]] && [[ "${ZFS_KEY_FILE}" == "empty" ]]
then
  echo '
  nofsck="yes"
  add_dracutmodules+=" zfs "
  omit_dracutmodules+=" btrfs "
  ' > /etc/dracut.conf.d/zol.conf

elif  [[ "${DO_ZFS_ENCRYPT}" == "yes" ]] && [[ "${ZFS_KEY_FILE}" != "empty" ]]
then
  echo '
  nofsck="yes"
  add_dracutmodules+=" zfs "
  omit_dracutmodules+=" btrfs "
  install_items+="'" ${ZFS_KEY_FILE} "'"
  ' > /etc/dracut.conf.d/zol.conf

elif [[ "${DO_ZFS_ENCRYPT}" == "no" ]]
then
  echo "no zfs encryption"
  echo '
  nofsck="yes"
  add_dracutmodules+=" zfs "
  omit_dracutmodules+=" btrfs "
  ' > /etc/dracut.conf.d/zol.conf

else
  echo "somethings broken here"
  exit 1
fi

# .Disable GPU drivers
echo 'omit_drivers+=" amdgpu radeon nvidia nouveau i915 "' >> /etc/zfsbootmenu/dracut.conf.d/drivers.conf

# Rebuild the initramfs
xbps-reconfigure -f linux6.8

#.Install and configure ZFSBootMenu
zfs set org.zfsbootmenu:commandline="ro quiet zbm.prefer=${ZPOOL_NAME} zbm.timeout=5" ${ZPOOL_NAME}/ROOT

echo '
Global:
  ManageImages: true
  BootMountPoint: /boot/efi
  DracutConfDir: /etc/zfsbootmenu/dracut.conf.d
  PreHooksDir: /etc/zfsbootmenu/generate-zbm.pre.d
  PostHooksDir: /etc/zfsbootmenu/generate-zbm.post.d
  InitCPIO: false
  InitCPIOConfig: /etc/zfsbootmenu/mkinitcpio.conf
Components:
  ImageDir: /boot/efi/EFI/void
  Versions: 3
  Enabled: true
  syslinux:
    Config: /boot/syslinux/syslinux.cfg
    Enabled: false
EFI:
  ImageDir: /boot/efi/EFI/void
  Versions: false
  Enabled: true
Kernel:
  CommandLine: ro quiet
' > /etc/zfsbootmenu/config.yaml

xbps-reconfigure -f zfsbootmenu

refind-install
[[ -f /boot/refind_linux.conf ]] && rm /boot/refind_linux.conf

# Create /boot/efi/EFI/void/refind_linux.conf:
cat << EOF > /boot/efi/EFI/void/refind_linux.conf
timeout 0
"Boot default"  "zbm.prefer=${ZPOOL_NAME} ro quiet loglevel=0 zbm.skip"
"Boot to menu"  "zbm.prefer=${ZPOOL_NAME} ro quiet loglevel=0 zbm.show"
EOF

exit 0