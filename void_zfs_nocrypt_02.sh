#!/bin/bash -x

# Version: 2022.11.23

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob


# general
export REPO="https://alpha.de.repo.voidlinux.org/current"
export ARCH="x86_64"

export XBPS_ARCH="${ARCH}"
export XBPS_INSTALL="$(which xbps-install)"

export MNT_CHROOT="/mnt"
export PARTPROBE="$(which partprobe)"

# define labels
HDD="$(cat hdd_linux)"
export EFI_HDD="$(blkid -s PARTUUID -o value ${HDD}-part1)"
export SWAPPY="$(blkid -s PARTUUID -o value ${HDD}-part2)"
export ZFS_SYSTEM="$(blkid -s PARTUUID -o value ${HDD}-part3)"

export HOST_NAME="x270"
export ZPOOL_NAME="zroot"
export HOST_NAME="x270"

# mount efi dir
mount -t efivarfs none /sys/firmware/efi/efivars

# Set the keymap, timezone and hardware clock
cat << EOF >> /etc/rc.conf
KEYMAP="de"
TIMEZONE="Europe/Berlin"
HARDWARECLOCK="UTC"
EOF

# set vconsole
cat << EOF > /etc/vconsole.conf
KEYMAP=de-latin1-nodeadkeys
FONT=lat9w-16
FONT_MAP=8859-1_to_uni
EOF

# Configure your glibc locale
cat << EOF >> /etc/default/libc-locales
en_US.UTF-8 UTF-8
en_US ISO-8859-1
de_DE.UTF-8 UTF-8
de_DE ISO-8859-1
de_DE@euro ISO-8859-15
EOF

# Add keymap to dracut
mkdir -p /etc/zfsbootmenu/dracut.conf.d
cat > /etc/zfsbootmenu/dracut.conf.d/keymap.conf <<EOF
install_optional_items+=" /etc/cmdline.d/keymap.conf "
EOF

mkdir -p /etc/cmdline.d/
cat > /etc/cmdline.d/keymap.conf <<EOF
rd.vconsole.keymap=de
EOF

# reconfigure glibc-locales
xbps-reconfigure -f glibc-locales

# set hostname
echo ${HOST_NAME} > /etc/hostname

# Set a root password
echo "set root's password"
passwd

# To more quickly discover and import pools on boot, we need to set a pool cachefile
zpool set cachefile=/etc/zfs/zpool.cache ${ZPOOL_NAME}

# Configure our default boot environment
zpool set bootfs=${ZPOOL_NAME}/ROOT/void ${ZPOOL_NAME}

# Configure Dracut to load ZFS support
cat << EOF > /etc/dracut.conf.d/zol.conf
nofsck="yes"
add_dracutmodules+=" zfs "
omit_dracutmodules+=" btrfs "
EOF

# .Disable GPU drivers
echo 'omit_drivers+=" amdgpu radeon nvidia nouveau i915 "' >> /etc/zfsbootmenu/dracut.conf.d/drivers.conf

# Rebuild the initramfs
xbps-reconfigure -f linux6.0

# Install and configure ZFSBootMenu
zfs set org.zfsbootmenu:commandline="rw loglevel=4" ${ZPOOL_NAME}/ROOT
#zfs set org.zfsbootmenu:commandline="ro quiet" ${ZPOOL_NAME}/ROOT

# Create a vfat filesystem
mkfs.vfat -F32 ${HDD}-part1

# .Create an fstab entry and mount
cat << EOF >> /etc/fstab
UUID=$(blkid -o value -s UUID ${HDD}-part1) /boot/efi vfat defaults 0 0
/dev/mapper/swappy  none   swap    defaults   0       0
EOF

# setup encrypted swap
echo "swappy        PARTUUID=${SWAPPY} /dev/urandom   swap,cipher=aes-xts-plain64,size=256" >> /etc/crypttab

mkdir -p /boot/efi
mount /boot/efi

# Install ZFSBootMenu
mkdir -p /boot/efi/EFI/void

# .Enable zfsbootmenu image creation
echo '
Global:
  ManageImages: true
  BootMountPoint: /boot/efi
  DracutConfDir: /etc/zfsbootmenu/dracut.conf.d
  PreHooksDir: /etc/zfsbootmenu/generate-zbm.pre.d
  PostHooksDir: /etc/zfsbootmenu/generate-zbm.post.d
  InitCPIO: false
  InitCPIOConfig: /etc/zfsbootmenu/mkinitcpio.conf
Components:
  ImageDir: /boot/efi/EFI/void
  Versions: 3
  Enabled: true
  syslinux:
    Config: /boot/syslinux/syslinux.cfg
    Enabled: false
EFI:
  ImageDir: /boot/efi/EFI/void
  Versions: false
  Enabled: true
Kernel:
  CommandLine: ro quiet loglevel=0
' > /etc/zfsbootmenu/config.yaml

# reconfigure zfsbootmenu
xbps-reconfigure -f zfsbootmenu

# rEFInd should automatically identify /boot/efi as your EFI partition and install itself accordingly.
refind-install
rm /boot/refind_linux.conf

# Create /boot/efi/EFI/void/refind_linux.conf:
cat << EOF > /boot/efi/EFI/void/refind_linux.conf
"Boot default"  "zbm.prefer=${ZPOOL_NAME} ro quiet loglevel=0 zbm.skip"
"Boot to menu"  "zbm.prefer=${ZPOOL_NAME} ro quiet loglevel=0 zbm.show"
EOF

efibootmgr -c -d ${HDD} -p 1 -L "ZFSBootMenu" -l '\EFI\void\vmlinuz-linux.EFI'
efibootmgr -c -d ${HDD} -p 1 -L "ZFSBootMenu" -l '\EFI\void\zfsbootmenu-release-vmlinuz-x86_64.EFI'

exit