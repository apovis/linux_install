export ROOTFS="btrfs"
export BOOTFS="btrfs"
export VARFS="btrfs"
export BOOTLOADER="grub"
MNT_CHROOT="/mnt"

setup-interfaces -ar 
setup-keymap us us-intl
setup-hostname
setup-timezone -z Europe/Berlin
setup-apkrepos -1
apk update

apk add bash rsync lvm2 cryptsetup e2fsprogs btrfs-progs f2fs-tools sgdisk parted mkinitfs findmnt lsblk vim curl tmux grub git grub-efi efibootmgr util-linux pciutils usbutils coreutils binutils findutils grep iproute2

passwd root
rc-update add networking boot
rc-update add urandom boot
setup-sshd -c openssh
setup-ntp chrony






chroot:

mount -t proc /proc ${MNT_CHROOT}/proc
mount --rbind /dev ${MNT_CHROOT}/dev
mount --make-rslave ${MNT_CHROOT}/dev
mount --rbind /sys ${MNT_CHROOT}/sys
chroot /mnt


mount -t proc /proc /mnt/proc
mount --rbind /dev  /mnt/dev
mount --make-rslave /mnt/dev
mount --rbind /sys  /mnt/sys
chroot /mnt