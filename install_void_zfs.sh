#!/bin/bash

# Version: 2023.11.24

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

sys_prep(){
  source host.conf  
  source libs/general.conf       
  source libs/partitioning.conf    
}

source_configs(){                      
# source general settings and host.conf
  source libs/pkgs.conf    
  source libs/installed_void_setup.conf  
  source libs/zfs.conf
  source libs/chroot_common.conf
  source libs/chroot.conf  
}

# run all functions 
sys_prep
export_vars
select_os_drive
prep_void_iso_for_setup
prep_zfs_for_setup
load_keys
source_configs
set_proxy
zfs_format
create_zpool
create_datasets
export_import_zfs_pool
copy_void_keys
install_pkgs
setup_doas
copy_files_to_chroot
copy_void_files_to_chroot
set_void_etc_configs
set_static_network
check_for_libc_variant
zbm_keymap
init_void_chroot
format_config_mount_efi
encrypted_swap
chroot_void_zfs
lights_out
