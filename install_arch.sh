#!/bin/bash

# Version: 2023.11.24

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

disk_prep(){
  source host.conf                           
  source libs/general.conf
  source libs/partitioning.conf       
}

source_configs(){                      
# source general settings and host.conf
  source libs/pkgs.conf    
  source libs/chroot_common.conf  
  source libs/installed_arch_setup.conf  
}


# run all functions 
disk_prep
export_vars
get_netif_mac
set_mnt_opts4disk
select_os_drive
prep_arch_iso_for_setup
load_keys
source_configs
set_proxy
disc_layout
partitioning
export_uuids
install_pkgs
set_arch_etc_configs
setup_doas
backup_luks_header
check_for_grub
replace_arch_mkinitcpio
setup_systemd_resolved
copy_files_to_arch_chroot
chroot_arch
lights_out_liveos
