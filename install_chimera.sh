#!/bin/bash

# Version: 2023.11.09

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x


export CHROOT_MNT="/CHROOT"
mkdir -p ${CHROOT_MNT}
chmod 755 ${CHROOT_MNT}

export MKFS_VFAT="$(which mkfs.vfat)"
export MKFS_BTRFS="$(which mkfs.btrfs)"

CHIMERA_LIVE_INSTALL="$(which chimera-live-install)"
INSTALL_EXPORTS_FILE="${CHROOT_MNT}/root/INSTALL_EXPORTS"

echo "which hdd to use:"
read HDD


# my layout:
# /dev/sd?1     /boot   vfat
# /dev/sd?2     swap    swap
# /dev/sd?3     /   btrfs

export BOOT_HDD="${HDD}1"
export BOOT_UUID_HDD="$(blkid -o value -s UUID ${BOOT_HDD})"

export SWAP_HDD="${HDD}2"
export SWAP_PARTUUID_HDD="$(blkid -o value -s PARTUUID ${SWAP_HDD})"

export ROOT_HDD="${HDD}3"
export ROOT_UUID_HDD="$(blkid -o value -s UUID ${ROOT_HDD})"

${MKFS_VFAT} ${BOOT_HDD}
${MKFS_BTRFS} -fL os_sys ${ROOT_HDD}

mount ${ROOT_HDD} ${CHROOT_MNT}
mkdir -p ${CHROOT_MNT}/boot
mount ${BOOT_HDD} ${CHROOT_MNT}/boot

${CHIMERA_LIVE_INSTALL} ${CHROOT_MNT}

echo "BOOT_HDD=${HDD}1" >> ${INSTALL_EXPORTS_FILE}
echo "SWAP_HDD=${HDD}2" >> ${INSTALL_EXPORTS_FILE}
echo "ROOT_HDD=${HDD}3" >> ${INSTALL_EXPORTS_FILE}
echo "BOOT_UUID_HDD=$(blkid -o value -s UUID ${BOOT_HDD})" >> ${INSTALL_EXPORTS_FILE}
echo "SWAP_PARTUUID_HDD=$(blkid -o value -s PARTUUID ${SWAP_HDD})" >> ${INSTALL_EXPORTS_FILE}
echo "ROOT_UUID_HDD=$(blkid -o value -s UUID ${ROOT_HDD})" >> ${INSTALL_EXPORTS_FILE}


echo "UUID=${BOOT_UUID_HDD} /boot vfat defaults     0 2" >> ${CHROOT_MNT}/etc/fstab
echo "UUID=${ROOT_UUID_HDD} / btrfs rw,noatime,compress=zstd:4,space_cache=v2,commit=120      0 0" >> ${CHROOT_MNT}/etc/fstab
echo "/dev/mapper/swappy none   swap    defaults    0 0" >> ${CHROOT_MNT}/etc/fstab

echo "swappy        PARTUUID=${SWAP_PARTUUID_HDD} /dev/urandom   swap,cipher=aes-xts-plain64:whirlpool,size=512" >> ${CHROOT_MNT}/etc/crypttab