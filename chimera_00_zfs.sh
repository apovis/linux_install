#!/usr/bin/env sh

# Version: 2024.06.28

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

apk update
apk upgrade --no-interactive openssl 
apk add --no-interactive bash tmux git

git clone https://gitlab.com/apovis/linux_install.git
cd linux_install
git switch simpler_scripts