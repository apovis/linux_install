#!/bin/bash

# Version: 2024.01.12

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x


if [[ ${SYSTEMD_EFI_BOOT_MANAGER_ENABLE} == "yes" ]] && [[ ${DO_CRYPT_HDD} == "crypt" ]]
then
  mkdir -p /boot/loader/entries
  bootctl install
  cp /root/linux_install/files/archlinux/boot_loader_loader.conf /boot/loader/loader.conf
  cp /root/linux_install/files/archlinux/boot_loader_entries_arch.conf /boot/loader/entries/arch.conf
  cp /root/linux_install/files/archlinux/boot_loader_entries_arch-fallback.conf /boot/loader/entries/arch-fallback.conf

  if [[ ${HDD_TYPE} == "ssd" ]] 
  then 
    export BOOT_LOADER_OPTS_CRYPT="options rd.luks.uuid=${ROOT_UUID} cryptdevice=UUID=${ROOT_UUID}:root:allow-discards root=UUID=${LUKS_UUID_ROOT} rootflags=subvol=/${ZROOT} rd.luks.options=discard rw"
  else
    export BOOT_LOADER_OPTS_CRYPT="options rd.luks.uuid=${ROOT_UUID} cryptdevice=UUID=${ROOT_UUID}:root root=UUID=${LUKS_UUID_ROOT} rootflags=subvol=/${ZROOT} rd.luks.options=discard rw"
  fi
  
  echo "${BOOT_LOADER_OPTS_CRYPT}" >> /boot/loader/entries/arch.conf
  echo "${BOOT_LOADER_OPTS_CRYPT}" >> /boot/loader/entries/arch-fallback.conf

  systemctl enable systemd-boot-update.service
  mkinitcpio -P

elif [[ ${SYSTEMD_EFI_BOOT_MANAGER_ENABLE} == "yes" ]] && [[ ${DO_CRYPT_HDD} == "nocrypt" ]]
then
  mkdir -p /boot/loader/entries
  bootctl install
  cp /root/linux_install/files/archlinux/boot_loader_loader.conf /boot/loader/loader.conf
  cp /root/linux_install/files/archlinux/boot_loader_entries_arch.conf /boot/loader/entries/arch.conf
  cp /root/linux_install/files/archlinux/boot_loader_entries_arch-fallback.conf /boot/loader/entries/arch-fallback.conf

  BOOT_LOADER_OPTS_NOCRYPT="options root=UUID=${ROOT_UUID} rootflags=subvol=/${ZROOT} rw"
  echo "${BOOT_LOADER_OPTS_NOCRYPT}" >> /boot/loader/entries/arch.conf
  echo "${BOOT_LOADER_OPTS_NOCRYPT}" >> /boot/loader/entries/arch-fallback.conf

  systemctl enable systemd-boot-update.service
  mkinitcpio -P

elif [[ ${SYSTEMD_EFI_BOOT_MANAGER_ENABLE} == "no" ]] && [[ ${FULL_DISC_ENCRYPTION} == "yes" ]]
then 
  echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
  grub-install --target=x86_64-efi --efi-directory=/efi --boot-directory=/boot --bootloader-id=archlinux --recheck
  grub-mkconfig -o /boot/grub/grub.cfg
  mkinitcpio -P

elif [[ ${SYSTEMD_EFI_BOOT_MANAGER_ENABLE} == "no" ]] && [[ ${FULL_DISC_ENCRYPTION} == "no" ]]
then 
  grub-install --target=x86_64-efi --efi-directory=/efi --boot-directory=/boot --bootloader-id=archlinux --recheck
  grub-mkconfig -o /boot/grub/grub.cfg
  mkinitcpio -P

else
  echo "something is wrong here!"
fi

# etc_files
cp /root/linux_install/files/archlinux/locale.gen /etc/locale.gen
locale-gen
cp /etc/makepkg.conf{,.orig}
cp /root/linux_install/files/archlinux/makepkg.conf /etc/makepkg.conf
cp /root/linux_install/files/archlinux/timesyncd.conf /etc/systemd/

# ssh files
cp /etc/ssh/sshd_config{,.orig}
cp /root/linux_install/files/sshd_config /etc/ssh/sshd_config
sed -i "s|<USERNAME>|${USERNAME}|g" "/etc/ssh/sshd_config"
cp /root/linux_install/files/keys /etc/ssh/keys.root
chmod 600 /etc/ssh/keys.root
cp /root/linux_install/files/keys /etc/ssh/keys.${USERNAME}
chmod 644 /etc/ssh/keys.${USERNAME}

# systemd-networkd
cp /root/linux_install/files/archlinux/10-ethernet.network /etc/systemd/network/10-${NET_IF}.network
sed -i "s|NET_IF_MAC|${NET_IF_MAC}|" "/etc/systemd/network/10-${NET_IF}.network"
sed -i "s|SD_ADDRESS|${SD_ADDRESS}|" "/etc/systemd/network/10-${NET_IF}.network"
sed -i "s|SD_GATEWAY|${SD_GATEWAY}|" "/etc/systemd/network/10-${NET_IF}.network"
sed -i "s|SD_DOMAINS|${SD_DOMAINS}|" "/etc/systemd/network/10-${NET_IF}.network"
sed -i "s|SD_DNS1|${SD_DNS1}|" "/etc/systemd/network/10-${NET_IF}.network"
sed -i "s|SD_DNS2|${SD_DNS2}|" "/etc/systemd/network/10-${NET_IF}.network"

# users and pwds
id -u ${USERNAME} &>/dev/null || useradd -m -s /bin/bash -G wheel -U ${USERNAME}
#echo "${USERNAME}:${USERNAME_PASSWD}" | chpasswd -c SHA512 -s 195000 
echo "${USERNAME}:${USERNAME_PASSWD}" | chpasswd -c YESCRYPT -s 195000 
#echo "root:${ROOT_PASSWD}" | chpasswd -c SHA512 -s 195000
echo "root:${ROOT_PASSWD}" | chpasswd -c YESCRYPT -s 195000

# bashrc
cp /root/linux_install/files/archlinux/bashrc_root /root/.bashrc
cp /root/linux_install/files/archlinux/bashrc_USER /home/${USERNAME}/.bashrc

# common configs
# tmux system-wide
cp /root/linux_install/files/tmux.conf /etc/tmux.conf
  
# bash_profile for root
cp /root/linux_install/files/bash_profile /root/.bash_profile

# bash_profile for your user
cp /root/linux_install/files/bash_profile /home/${USERNAME}/.bash_profile

# foot config for user
mkdir -p /home/${USERNAME}/.config/foot
cp /root/linux_install/files/foot_* /home/${USERNAME}/.config/foot
ln -s /home/${USERNAME}/.config/foot/foot_dark.ini /home/${USERNAME}/.config/foot/foot.ini

# neovim
cd /tmp
git clone https://gitlab.com/apovis/vim.git
curl -fLo "${XDG_DATA_HOME:-/root/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir -p /root/.config/nvim/
cp vim/vimrc /root/.config/nvim/init.vim 
cp -a vim/autoload /root/.local/share/nvim/site/
mkdir -p /home/${USERNAME}/.local/share/nvim/site/ /root/.local/share/nvim/site/
curl -fLo "${XDG_DATA_HOME:-/home/${USERNAME}/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cp -a vim/autoload /home/${USERNAME}/.local/share/nvim/site/ 
mkdir -p /home/${USERNAME}/.config/nvim/
cp vim/vimrc /home/${USERNAME}/.config/nvim/init.vim
chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}

# encrypt swap
echo "swappy        PARTUUID=$(blkid -s PARTUUID -o value ${SWAP_HDD}) /dev/urandom   swap,cipher=aes-xts-plain64:whirlpool,size=512" >> /etc/crypttab
echo "/dev/mapper/swappy  none   swap    defaults   0       0" >> /etc/fstab

# services
systemctl enable sshd.service systemd-timesyncd.service systemd-networkd.service systemd-resolved.service
pkgfile --update