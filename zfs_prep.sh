#!/bin/bash -x

# Version: 2022.11.23

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob

# not for chroot

# general
export REPO="https://alpha.de.repo.voidlinux.org/current"
export ARCH="x86_64"

export XBPS_ARCH="${ARCH}"
export XBPS_INSTALL="$(which xbps-install)"
${XBPS_INSTALL} -Syu xbps
${XBPS_INSTALL} -S tmux

echo "which drive should be cleaned?"

select DRIVE in $(ls /dev/disk/by-id/);
do
  export HDD="/dev/disk/by-id/${DRIVE}"
  break
done

swapoff --all
zfs umount -a
zpool export -a