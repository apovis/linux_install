#!/usr/bin/env bash

# Version: 2024.07.05

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

# setting ZFSBootMenu properties
zfs set org.zfsbootmenu:commandline="ro quiet zbm.prefer=${ZPOOL_NAME} zbm.timeout=5" ${ZPOOL_NAME}/ROOT

echo "what's the machines name?"
read HOST_NAME
echo ${HOST_NAME} >/etc/hostname

# locales settings
echo '
de_DE.UTF-8 UTF-8  
de_DE ISO-8859-1  
de_DE@euro ISO-8859-15  
en_US.UTF-8 UTF-8  
en_US ISO-8859-1
' >>/etc/locale.gen
locale-gen

# set localtime
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# mount efivars
mount -t efivarfs efivarfs /sys/firmware/efi/efivars || echo "already mounted"

# set ftab
echo "efivarfs /sys/firmware/efi/efivars efivarfs defaults 0 0" > /etc/fstab
echo "UUID=$(blkid -o value -s UUID ${BOOT_DEVICE}) /boot/efi vfat defaults 0 2" >> /etc/fstab

# zfsbootmenu
mkdir -p /etc/zfsbootmenu/
cat <<EOF >/etc/zfsbootmenu/config.yaml
Global:
  ManageImages: true
  BootMountPoint: /boot/efi
  DracutConfDir: /etc/zfsbootmenu/dracut.conf.d
  PreHooksDir: /etc/zfsbootmenu/generate-zbm.pre.d
  PostHooksDir: /etc/zfsbootmenu/generate-zbm.post.d
  InitCPIO: true
  InitCPIOConfig: /etc/zfsbootmenu/mkinitcpio.conf
Components:
  ImageDir: /boot/efi/EFI/${DISTRO}
  Versions: 3
  Enabled: false
  syslinux:
    Config: /boot/syslinux/syslinux.cfg
    Enabled: false
EFI:
  ImageDir: /boot/efi/EFI/${DISTRO}
  Versions: false
  Enabled: true
Kernel:
  CommandLine: ro quiet loglevel=0
EOF

# gen zbm config
generate-zbm

# make a backup
cp /boot/efi/EFI/${DISTRO}/vmlinuz-linux.EFI /boot/efi/EFI/${DISTRO}/vmlinuz-linux-backup.EFI 

# set boot params
zfs set org.zfsbootmenu:commandline="rw quiet zbm.prefer=${ZPOOL_NAME} zbm.timeout=5" ${ZPOOL_NAME}/ROOT

# writting entry for zbm in efibootmgr
efibootmgr --create --disk ${OS_DISK} --part ${BOOT_PART} --label "ZFSBootMenu" --loader "\\EFI\\${DISTRO}\\vmlinuz-linux.EFI" --unicode
efibootmgr --create --disk ${OS_DISK} --part ${BOOT_PART} --label "ZFSBootMenu (Backup)" --loader "\\EFI\\${DISTRO}\\vmlinuz-linux-backup.EFI" --unicode

#.To more quickly discover and import pools on boot, we need to set a pool cachefile
zpool set cachefile=/etc/zfs/zpool.cache ${ZPOOL_NAME}

# change root's shell
echo "changing password for root!"
passwd root

# add dataset user DEFAULT
zfs create -o mountpoint=/home/apovis ${ZPOOL_NAME}/home/${USERNAME}

# add user
useradd -m -U -s /bin/bash -G wheel ${USERNAME}
echo "setting password for ${USERNAME}"
passwd ${USERNAME}

# enable zfs services
systemctl enable zfs-import-cache.service zfs-mount.service zfs-import.target sshd.service systemd-timesyncd.service systemd-networkd.service systemd-resolved.service

# gen initramfs
mkinitcpio -P

# leave chroot
exit 0
