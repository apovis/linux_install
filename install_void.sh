#!/bin/bash

# Version: 2024.09.14

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

sys_prep(){
  source host.conf                     
  source libs/general.conf 
}

source_configs(){                      
# source general settings and host.conf
  source libs/partitioning.conf
  source libs/pkgs.conf    
  source libs/installed_void_setup.conf  
  source libs/chroot_common.conf  
  source libs/chroot.conf  
}


# run all functions 
sys_prep
export_vars
select_os_drive
prep_void_iso_for_setup
load_keys
source_configs
set_mnt_opts4disk
set_proxy
disc_layout
partitioning
# delete the long version
export_uuids
copy_void_keys
install_pkgs
setup_doas
backup_luks_header
set_void_grub_config
copy_linux_install_to_chroot
#encrypt_swap
init_void_chroot
chroot_void
#lights_out_liveos