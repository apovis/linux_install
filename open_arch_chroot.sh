#!/bin/bash

# Version: 2023.07.31

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x


export HDD="/dev/sd"
export LUKS_NAME="luks-$(cryptsetup luksUUID ${HDD}4)"
export LUKS_DEVICE="/dev/mapper/${LUKS_NAME}"

MOUNT_OPTS="rw,relatime,ssd,space_cache=v2,commit=120,compress=zstd:4"
MNT_CHROOT="/mnt"

cryptsetup luksOpen ${HDD}4 ${LUKS_NAME}
mount ${LUKS_DEVICE} -o ${MOUNT_OPTS},subvol=/root ${MNT_CHROOT}
mount ${HDD}1 ${MNT_CHROOT}/efi
mount ${HDD}2 -o ${MOUNT_OPTS},subvol=boot01 ${MNT_CHROOT}/boot
mount ${LUKS_DEVICE} -o ${MOUNT_OPTS},subvol=/roothome ${MNT_CHROOT}/root      
mount ${LUKS_DEVICE} -o ${MOUNT_OPTS},subvol=/home ${MNT_CHROOT}/home 
mount ${LUKS_DEVICE} -o ${MOUNT_OPTS},subvol=/snapshots ${MNT_CHROOT}/snapshots

arch-chroot ${MNT_CHROOT}

exit 0