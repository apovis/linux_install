# Version: 2024.09.14

mount_efivarfs(){
  mount -t efivarfs none /sys/firmware/efi/efivars
}

install_void_grub(){
  grub-install --target=x86_64-efi --efi-directory=/boot --boot-directory=/boot --bootloader-id=voidlinux --recheck
  grub-mkconfig -o /boot/grub/grub.cfg
}


create_void_fstab(){
    echo "UUID=${BOOT_UUID} $(findmnt -ren --output=target,fstype,options -T /boot) 0 0" >> /etc/fstab
    echo "UUID=${EFI_UUID} $(findmnt -ren --output=target,fstype,options -T /efi) 0 0" >> /etc/fstab
    for i in /root /var /var/cache /var/log /home /snapshots /srv
    do 
      echo "UUID=${ROOT_UUID} $(findmnt -ren --output=target,fstype,options -T ${i}) 0 0" >> /etc/fstab
    done
    echo "efivarfs /sys/firmware/efi/efivars efivarfs defaults 0 0" >> /etc/fstab
}


copy_void_etc_files(){
  #passwd
  chsh -s /bin/bash
  ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
  echo "LANG=en_US.UTF-8" > /etc/locale.conf
  echo "LANG=de_DE.UTF-8" >> /etc/locale.conf
  xbps-reconfigure -fa
  echo ${HOST_NAME} > /etc/hostname
}


copy_void_bashrc(){
  cp /root/linux_install/files/voidlinux/bashrc_root /root/.bashrc
  cp /root/linux_install/files/voidlinux/bashrc_USER /home/${USERNAME}/.bashrc
}


enable_void_services(){
  #mkdir -p /var/service
  #chmod 644 /var/service
  #chown -R root.root /var/service
  #ln -s /etc/sv/dhcpcd-eth0 /etc/runit/runsvdir/default/
  ln -s /etc/sv/sshd /etc/runit/runsvdir/default/
  ln -s /etc/sv/openntpd /etc/runit/runsvdir/default/
  ln -s /etc/sv/crond /etc/runit/runsvdir/default/
  ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
  ln -s /etc/sv/acpid /etc/runit/runsvdir/default/
  ln -s /etc/sv/socklog-unix /etc/runit/runsvdir/default/
}


libc_addon(){
  if [[ ${LIBC_VARIANT} == "glibc" ]]
  then
    ${XBPS_INSTALL} -S glibc-locales
    echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales
    echo "de_DE.UTF-8 UTF-8" >> /etc/default/libc-locales
    xbps-reconfigure -f glibc-locales
  elif [[ ${LIBC_VARIANT} == "musl" ]]
  then
    echo "no glibc needed"

  else
    echo "something is broken here."
    exit 1
  fi
}


add_more_groups_to_user(){
  
  case ${SERVER_PROFILE} in

    void_desktop)
      usermod -aG wheel,users,audio,video,storage,network,scanner,_seatd ${USERNAME} ;;

    void_base)
      usermod -aG wheel,users,storage,network ${USERNAME} ;;

    void_server)
      usermod -aG wheel,users,storage,network ${USERNAME} ;;

    *)
      echo "something went wrong here" ;;

    esac
}
