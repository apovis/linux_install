#!/bin/bash

# Version: 2023.11.24

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

disk_prep(){
  source host.conf         
  source libs/alpine.conf            
  source libs/general.conf       
  source libs/partitioning.conf   
}


source_configs(){                      
# source general settings and host.conf
  source libs/pkgs.conf    
  source libs/installed_alpine_setup.conf  
  source libs/chroot_common.conf 
}


# run all functions 
disk_prep
alpine_select_disk
#select_os_drive
export_vars
source_configs
set_proxy
set_mnt_opts4disk
#disc_layout
#partitioning
alpine_4parts_format
set_root_device
btrfs_mount_nosubs
install_pkgs
setup_services
install_base_to_chroot
#set_alpine_mkinitfs
set_alpine_grub_config
#set_alpine_extlinux_config
export_uuids
backup_luks_header
set_alpine_mkinitfs
copy_files_to_chroot
#msg_4_alpine_chroot
init_alpine_chroot
chroot_alpine
