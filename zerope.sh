#!/bin/bash

# Version: 2024.01.12

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

DREAM_CA_URL="https://www.grafmurr.de/nspawns/dreamca.pem"
DREAM_CA="dreamca.pem"


# get pem
curl -LOC - ${DREAM_CA_URL}
trust anchor --store ${DREAM_CA}
update-ca-trust

echo '

[zerope]
SigLevel = Optional TrustAll
Server = https://beastmon01.dreaming.void/zerope

' >> /etc/pacman.conf