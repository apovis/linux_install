#!/bin/bash

# Version: 2023.11.10

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x


# mount_efivarfs
#mount -t efivarfs none /sys/firmware/efi/efivars

# set_alpine_grub_config
mkdir -p /etc/default/
cp /etc/default/grub{,.orig}
cp /root/linux_install/files/alpine/grub_alpine /etc/default/grub

if [[ ${DO_CRYPT_HDD} == "crypt" ]] 
then
  sed -i "s|<WILL-BE-CHANGED>|cryptroot=UUID=${ROOT_UUID} cryptdm=${LUKS_NAME} |" "/etc/default/grub"
else
  sed -i "s|<WILL-BE-CHANGED>||" "/etc/default/grub"
fi

# install_alpine_grub
grub-install --target=x86_64-efi --efi-directory=/efi --boot-directory=/boot --bootloader-id=alpine --recheck
grub-mkconfig -o /boot/grub/grub.cfg

# copy_ssh_files
cp /etc/ssh/sshd_config{,.orig}
cp /root/linux_install/files/sshd_config /etc/ssh/sshd_config
sed -i "s|<USERNAME>|${USERNAME}|g" "/etc/ssh/sshd_config"
install -m 600 /root/linux_install/files/keys /etc/ssh/keys.root
install -m 644 /root/linux_install/files/keys /etc/ssh/keys.${USERNAME}

# create_user_and_set_pwds
id -u ${USERNAME} &>/dev/null || useradd -m -s /bin/bash -G wheel -U ${USERNAME}
echo "${USERNAME}:${USERNAME_PASSWD}" | chpasswd -c SHA512 -s 195000 
echo "root:${ROOT_PASSWD}" | chpasswd -c SHA512 -s 195000

# encrypt_swap
echo "swappy        PARTUUID=$(blkid -s PARTUUID -o value ${SWAP_HDD}) /dev/urandom   swap,cipher=aes-xts-plain64:whirlpool,size=512" >> /etc/crypttab
echo "/dev/mapper/swappy  none   swap    defaults   0       0" >> /etc/fstab

# copy_common_configs
# tmux system-wide
cp /root/linux_install/files/tmux.conf /etc/tmux.conf
  
# bash_profile for root
cp /root/linux_install/files/bash_profile /root/.bash_profile

# bash_profile for your user
cp /root/linux_install/files/bash_profile /home/${USERNAME}/.bash_profile
chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}/

# neovim setup
cd /tmp
git clone https://gitlab.com/apovis/vim.git
curl -fLo "${XDG_DATA_HOME:-/root/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir -p /root/.config/nvim/
cp vim/vimrc /root/.config/nvim/init.vim 
cp -a vim/autoload /root/.local/share/nvim/site/
mkdir -p /home/${USERNAME}/.local/share/nvim/site/ /root/.local/share/nvim/site/
curl -fLo "${XDG_DATA_HOME:-/home/${USERNAME}/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cp -a vim/autoload /home/${USERNAME}/.local/share/nvim/site/ 
mkdir -p /home/${USERNAME}/.config/nvim/
cp vim/vimrc /home/${USERNAME}/.config/nvim/init.vim
chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}