#!/bin/bash

# Version: 2024.09.14

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x


# set_void_etc_configs
ln -sf /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime
echo "${HOST_NAME}" > /etc/hostname
  
echo "LANG=${LANG}
LC_COLLATE=C
" >> /etc/locale.conf
  
echo "KEYMAP=${VCONSOLE_KEYMAP}
KEYMAP_TOGGLE=us
FONT=Lat2-Terminus16
" >> /etc/vconsole.conf

echo 'KEYMAP="de"
TIMEZONE="Europe/Berlin"
HARDWARECLOCK="UTC"
' >> /etc/rc.conf

# set_static_network
echo "ip link set dev ${NET_IF} up
ip addr add ${SD_ADDRESS} brd + dev ${NET_IF}
ip route add default via ${SD_GATEWAY}" >> /etc/rc.local

grub-install --target=x86_64-efi --efi-directory=/boot --boot-directory=/boot --bootloader-id=voidlinux --recheck
grub-mkconfig -o /boot/grub/grub.cfg

echo "LANG=en_US.UTF-8" >> /etc/locale.conf

#echo "UUID=${BOOT_UUID} $(findmnt --real --output=target,fstype,options -T /boot | tail -n +2) 0 0" >> /etc/fstab
#echo "UUID=${EFI_UUID} $(findmnt --real --output=target,fstype,options -T /efi | tail -n +2) 0 0" >> /etc/fstab
echo "UUID=${EFI_UUID} $(findmnt --real --output=target,fstype,options -T /boot | tail -n +2) 0 0" >> /etc/fstab
for i in / /root /home /snapshots 
do 
  echo "UUID=${OS_UUID} $(findmnt --real --output=target,fstype,options -T ${i} | tail -n +2) 0 0" >> /etc/fstab
done
echo "efivarfs /sys/firmware/efi/efivars efivarfs defaults 0 0" >> /etc/fstab

if [[ ${LIBC_VARIANT} == "glibc" ]]
then
  ${XBPS_INSTALL} -S glibc-locales
  echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales
  echo "de_DE.UTF-8 UTF-8" >> /etc/default/libc-locales
  xbps-reconfigure -f glibc-locales
elif [[ ${LIBC_VARIANT} == "musl" ]]
then
  echo "no glibc needed"
else
  echo "something is broken here."
  exit 1
fi

chsh -s /bin/bash
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "LANG=de_DE.UTF-8" >> /etc/locale.conf
xbps-reconfigure -fa
echo ${HOST_NAME} > /etc/hostname

cp /etc/ssh/sshd_config{,.orig}
cp /root/linux_install/files/sshd_config /etc/ssh/sshd_config
sed -i "s|<USERNAME>|${USERNAME}|g" "/etc/ssh/sshd_config"

cp /root/linux_install/files/keys /etc/ssh/keys.root
chmod 600 /etc/ssh/keys.root

cp /root/linux_install/files/keys /etc/ssh/keys.${USERNAME}
chmod 644 /etc/ssh/keys.${USERNAME}

id -u ${USERNAME} &>/dev/null || useradd -m -s /bin/bash -G wheel -U ${USERNAME}

echo "${USERNAME}:${USERNAME_PASSWD}" | chpasswd -c SHA512 -s 195000 
echo "root:${ROOT_PASSWD}" | chpasswd -c SHA512 -s 195000

# copy bashrcs
cp /root/linux_install/files/bash_profile /root/.bash_profile
cp /root/linux_install/files/voidlinux/bashrc_root /root/.bashrc

cp /root/linux_install/files/bash_profile /home/${USERNAME}/.bash_profile
cp /root/linux_install/files/voidlinux/bashrc_USER /home/${USERNAME}/.bashrc


case ${SERVER_PROFILE} in

  void_desktop)
    usermod -aG wheel,users,audio,video,storage,network,scanner,_seatd ${USERNAME} ;;

  void_base)
    usermod -aG wheel,users,storage,network ${USERNAME} ;;

  void_server)
    usermod -aG wheel,users,storage,network ${USERNAME} ;;

  *)
    echo "something went wrong here" ;;

esac

chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}

echo "swappy        PARTUUID=$(blkid -s PARTUUID -o value ${SWAP_HDD}) /dev/urandom   swap,cipher=aes-xts-plain64:whirlpool,size=512" >> /etc/crypttab
echo "/dev/mapper/swappy  none   swap    defaults   0       0" >> /etc/fstab

# tmux system-wide
cp /root/linux_install/files/tmux.conf /etc/tmux.conf
  
# chown for home dir
chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}/

ln -s /etc/sv/sshd /etc/runit/runsvdir/default/
ln -s /etc/sv/openntpd /etc/runit/runsvdir/default/
ln -s /etc/sv/crond /etc/runit/runsvdir/default/
ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
ln -s /etc/sv/acpid /etc/runit/runsvdir/default/
ln -s /etc/sv/socklog-unix /etc/runit/runsvdir/default/


exit 0