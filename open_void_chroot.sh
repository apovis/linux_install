#!/bin/bash

# Version: 2022.08.09

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

cryptsetup luksOpen /dev/sda3 matrix230
mkdir bla
mount /dev/mapper/matrix230 /bla/
mount /dev/mapper/matrix230 -o rw,relatime,ssd,space_cache=v2,commit=120,compress=zstd,subvol=root /mnt
mount /dev/mapper/matrix230 -o rw,relatime,ssd,space_cache=v2,commit=120,compress=zstd,subvol=/var /mnt/var
mount /dev/mapper/matrix230 -o rw,relatime,ssd,space_cache=v2,commit=120,compress=zstd,subvol=/home /mnt/home/
mount /dev/mapper/matrix230 -o rw,relatime,ssd,space_cache=v2,commit=120,compress=zstd,subvol=/tmp /mnt/tmp/
mount /dev/mapper/matrix230 -o rw,relatime,ssd,space_cache=v2,commit=120,compress=zstd,subvol=/opt /mnt/opt
mount /dev/mapper/matrix230 -o rw,relatime,ssd,space_cache=v2,commit=120,compress=zstd,subvol=/.snapshots /mnt/.snapshots
umount /bla
mount /dev/sda2 /mnt/boot/
mount /dev/sda1 /mnt/boot/efi/

export MNT_CHROOT="/mnt"

cp /etc/resolv.conf ${MNT_CHROOT}/etc/
mount --rbind /sys ${MNT_CHROOT}/sys 
mount --make-rslave ${MNT_CHROOT}/sys
mount --rbind /dev ${MNT_CHROOT}/dev 
mount --make-rslave ${MNT_CHROOT}/dev
mount --rbind /proc ${MNT_CHROOT}/proc 
mount --make-rslave ${MNT_CHROOT}/proc

chroot /mnt /bin/bash