#!/bin/bash -x

# Version: 2022.11.29

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob

export MNT_CHROOT="/mnt"
export ZPOOL_NAME="zroot"

zgenhostid -f

select_hdd(){
select DRIVE in $(ls /dev/disk/by-id/);
do
  export HDD="/dev/disk/by-id/${DRIVE}"
  echo "${HDD}" > /tmp/hdd_linux
  echo "OS will be installed on ${HDD}"
  break
done
}

mount_fs(){
  zpool import -f -N -R ${MNT_CHROOT} ${ZPOOL_NAME}
  zfs load-key -L prompt ${ZPOOL_NAME}
  zfs mount ${ZPOOL_NAME}/ROOT/void
  zfs mount ${ZPOOL_NAME}/home
  zfs mount ${ZPOOL_NAME}/home/root

  mount ${HDD}-part1 /mnt/boot/efi
}

init_chroot(){
  mount --rbind /sys ${MNT_CHROOT}/sys 
  mount --make-rslave ${MNT_CHROOT}/sys
  mount --rbind /dev ${MNT_CHROOT}/dev 
  mount --make-rslave ${MNT_CHROOT}/dev
  mount --rbind /proc ${MNT_CHROOT}/proc 
  mount --make-rslave ${MNT_CHROOT}/proc
}

lights_out(){
  # unmount everything
  umount /mnt/boot/efi
  umount -R ${MNT_CHROOT}
  zfs umount -a

  # export zpool and reboot
  zpool export -a
  poweroff
}


# run functions
select_hdd
mount_fs
init_chroot