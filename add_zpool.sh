#!/usr/bin/env bash

# 2024.06.15

# add an extra zpool
export DATA_HDD="/dev/XXX"
export ZPOOL_NAME="abigroot"
export ZFS_KEY_LOCATION="file:///etc/zfs/${ZPOOL_NAME}.key"
export ZFS_KEY_FILE="/etc/zfs/${ZPOOL_NAME}.key"
export DATA_HDD_PART="1"

# wipe hdd and create one partition
wipefs -a ${DATA_HDD}
sgdisk -Z ${DATA_HDD}
sgdisk -n "1:0:-10m" -t "1:bf00" ${DATA_HDD}

# get pwd for extra drive
echo "Set ZFS passphrase"
read -r -p "> ZFS passphrase: " -s ZROOT_PWD
echo ${ZROOT_PWD} > ${ZFS_KEY_FILE}
chmod 000 ${ZFS_KEY_FILE}

zpool create -f -o ashift=12 \
    -O acltype=posixacl \
    -O canmount=off \
    -O dnodesize=auto \
    -O relatime=on \
    -O xattr=sa \
    -O compression=zstd-4 \
    -o autotrim=on \
    -O normalization=formD \
    -O encryption=aes-256-gcm \
    -O keylocation="${ZFS_KEY_LOCATION}" \
    -O keyformat=passphrase \
    -O devices=off \
    -m none ${ZPOOL_NAME} ${DATA_HDD}${DATA_HDD_PART}

echo "zpool ${ZPOOL_NAME} has been created"
zpool list

exit 0