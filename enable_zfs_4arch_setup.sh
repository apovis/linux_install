#!/bin/bash

# Version: 2023.11.24
# purpose: setup local or remote repo for zfs

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x


# Variables
REPO=$1


# function definitions

set_local_repo() {
DREAMCA_PEM_URL="https://www.grafmurr.de/nspawns/dreamca.pem"
DREAMCA_PEM="dreamca.pem"

curl -o /tmp/${DREAMCA_PEM} ${DREAMCA_PEM_URL}
trust anchor --store /tmp/${DREAMCA_PEM}
update-ca-certificates

# local zerope
cat << EOF >> /etc/pacman.conf

[zerope]
SigLevel = Optional TrustAll
Server = https://beastmon01.dreaming.void/zerope/
EOF
}


set_archzfs_repo() {
# get gpg-key, import it and set a trust level (3 ;-)  )
ARCHZFS_GPG_KEY_URL="https://archzfs.com/archzfs.gpg"
ARCHZFS_GPG_KEY="/tmp/archzfs.gpg"
ARCHZFS_GPG_FINGERPRINT="DDF7DB817396A49B2A2723F7403BD972F75D9D76"

curl -o ${ARCHZFS_GPG_KEY} ${ARCHZFS_GPG_KEY_URL}
gpg --import ${ARCHZFS_GPG_KEY}
gpg --edit-key ${ARCHZFS_GPG_FINGERPRINT} trust quit


# set remote archzfs repo
cat << EOF >> /etc/pacman.conf

[archzfs]
SigLevel = Optional TrustAll
Include = /etc/pacman.d/archzfs.mirror
EOF

# mirrors of archzfs repo
cat << EOF > /etc/pacman.d/archzfs.mirror
# Origin Server - Finland
Server = http://archzfs.com/$repo/$arch
# Mirror - Germany
Server = http://mirror.sum7.eu/archlinux/archzfs/$repo/$arch
# Mirror - Germany
Server = http://mirror.sunred.org/archzfs/$repo/$arch
# Mirror - Germany
Server = https://mirror.biocrafting.net/archlinux/archzfs/$repo/$arch
# Mirror - India
Server = https://mirror.in.themindsmaze.com/archzfs/$repo/$arch
# Mirror - US
Server = https://zxcvfdsa.com/archzfs/$repo/$arch
EOF
}


set_repo() {
  case ${REPO} in

    local)
      set_local_repo
    ;;

    remote)
      set_archzfs_repo
    ;;

    *)
      echo "supported types are:
      - local
      - remote
      eg: $0 local|remote"
      exit 1
    ;;

  esac
}


# run funtions here
set_repo

echo "done setting up repo"

exit 0
