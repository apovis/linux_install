#!/usr/bin/env bash

# Version: 2024.08.30

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

# upgrade and install needed pkgs
apk update
apk upgrade --available --no-interactive
apk add --no-interactive chimera-repo-contrib
apk update
apk upgrade --available --no-interactive
apk add --no-interactive gptfdisk base-minimal base-files iputils findutils chimerautils cryptsetup-scripts linux-stable-zfs-bin linux-lts-zfs-bin tig tmux rsync htop curl git btrfs-progs zstd bash-completion chrony networkmanager neovim efibootmgr fonts-source-code-pro-otf initramfs-tools

# setting ZFSBootMenu properties
zfs set org.zfsbootmenu:commandline="ro quiet zbm.prefer=${ZPOOL_NAME} zbm.timeout=5" ${ZPOOL_NAME}/ROOT

# set keycaching IF encrypting and needed
if [ ${ZFS_KEY_CACHING} == "yes" ] && [ ${ENCRYPT_ZFS} == "yes" ]; then
  zfs set org.zfsbootmenu:keysource="${ZPOOL_NAME}/ROOT/${DISTRO}" ${ZPOOL_NAME}
elif [ ${ZFS_KEY_CACHING} == "no" ] && [ ${ENCRYPT_ZFS} == "yes" ]; then
  echo "no caching for keys"
else
  echo "please use yes or no for ENCRYPT_ZFS and ZFS_KEY_CACHING"
  exit 1
fi

# mount efivars
mount -t efivarfs efivarfs /sys/firmware/efi/efivars || echo "already mounted"

# set fstab
echo "efivarfs /sys/firmware/efi/efivars efivarfs defaults 0 0" >/etc/fstab
echo "UUID=$(blkid -o value -s UUID ${BOOT_DEVICE}) /boot/efi vfat defaults 0 2" >>/etc/fstab

echo "what's the machines name?"
read HOST_NAME
echo ${HOST_NAME} >/etc/hostname

# set localtime
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# change root's shell
chsh -s /bin/bash root
echo "changing password for root!"
passwd root

# add dataset, new user and set rights
zfs create -o mountpoint=/home/${USER_NAME} -o compression=zstd-5 ${ZPOOL_NAME}/home/${USER_NAME}
useradd -m -U -s /bin/bash -G wheel ${USER_NAME}
echo "setting password for ${USER_NAME}"
passwd ${USER_NAME}
chown -R ${USER_NAME}:${USER_NAME} /home/${USER_NAME}

# gen initramfs
update-initramfs -c -k all

# grub or zfsbootmenu
if [ ${EFI_BOOT_LOADER} == "yes" ]; then
  # Fetch a prebuilt ZFSBootMenu EFI executable, saving it to the EFI system partition:
  curl --create-dirs -o /boot/efi/EFI/${DISTRO}/VMLINUZ.EFI -LC - https://get.zfsbootmenu.org/latest.EFI
  cp /boot/efi/EFI/${DISTRO}/VMLINUZ.EFI /boot/efi/EFI/${DISTRO}/VMLINUZ-BACKUP.EFI
  # setup efi boot entries
  efibootmgr -c -d "${OS_DISK}" -p "${BOOT_PART}" -L "ZFSBootMenu (Backup)" -l "\\EFI\\${DISTRO}\\VMLINUZ-BACKUP.EFI"
  efibootmgr -c -d "${OS_DISK}" -p "${BOOT_PART}" -L "ZFSBootMenu" -l "\\EFI\\${DISTRO}\\VMLINUZ.EFI"
elif [ ${EFI_BOOT_LOADER} == "no" ]; then
  apk add --no-interactive grub
  grub-install --target=x86_64-efi --efi-directory=/boot/efi --boot-directory=/boot --bootloader-id=${DISTRO} --recheck
  update-grub
else 
  echo "please use yes or no for EFI_BOOT_LOADER"
fi

# leave chroot
exit 0
